const standardViewStyles = theme => ({
    lowerPaper: {
        margin: theme.spacing(1),
        color: "white",
        backgroundColor: theme.palette.primary.dark,
        overflow: "hidden"
    },
    viksDebut: {
        display: 'flex',
        marginTop: "10vh",
        height: '320px',
        width: '320px'
    },
    whitePaper: {
        margin: theme.spacing(2),
        padding: theme.spacing(2),
        color: "white",
        backgroundColor: "white",
        overflow: "hidden",
    },
    upperPaper: {
        margin: theme.spacing(2),
        color: "white",
        backgroundColor: theme.palette.info.main,
        overflow: "hidden"
    },
    airtableMain: {
        margin: theme.spacing(2),
        color: "white",
        backgroundColor: theme.palette.info.main,
        overflowY: "scroll",
        overflowX: "auto",
        width: "99%",
        height: "65vh"
    },
    simpleType: {
        marginLeft: theme.spacing(2),
        marginRight: theme.spacing(2),
        textAlign: 'center'
    },
    simpleTypeLeft: {
        marginLeft: theme.spacing(2),
        marginRight: theme.spacing(2),
        textAlign: 'left'
    },
    flexArea: {
        flexGrow: 1,
    },
    gridPaper: {
        marginTop: theme.spacing(0.5),
        marginBottom: theme.spacing(0.5),
        backgroundColor: theme.palette.primary.dark,
    },
    icon: {
        marginTop:  theme.spacing(1)
    },
    iconRight: {
        "&:hover": {
            backgroundColor: theme.palette.secondary.main,
            color: theme.palette.info.light
        },
        marginLeft:  theme.spacing(1),
        marginRight:  theme.spacing(6)
    },
    wideIMG: {
        marginTop: theme.spacing(1),
        width: "95%",
        height: "auto"
    },
    chip: {
        margin: theme.spacing(0.5)
    },
    dropZoneChip: {
        minWidth: "100%",
        maxWidth: "100%",
        margin: theme.spacing(0.25)
    },
    previewCard: {
        margin: theme.spacing(2),
        textAlign: 'center',
        color: "white",
        backgroundColor: theme.palette.info.main,
        overflow: "hidden"
    }, 
    previewCardSelected: {
        margin: theme.spacing(3),
        textAlign: 'center',
        color: "white",
        backgroundColor: theme.palette.primary.main,
        overflow: "hidden"
    },
    spacer: {
        margin: theme.spacing(2)
    },
    spacerMini: {
        margin: theme.spacing(0.5)
    },
    whiteColor: {
        color: "white"
    },
    scratchPaper: {
        width: "100%",
        color: "white",
        backgroundColor: theme.palette.primary.dark
    },
    fabProgress: {
        color: "white",
        position: 'absolute',
        top: -6,
        left: -6,
        zIndex: 1,
    },
    airtable: {
        overflowX: "scroll",
        margin: theme.spacing(2),
        backgroundColor: theme.palette.info.main
    },
    tableFlex: {
        display: 'flex',
        alignItems: 'center',
        flexWrap: 'wrap'
    },
    iconBump: {
        marginLeft: theme.spacing(1),
        "&:hover": {
            backgroundColor: theme.palette.primary.main
        }
    }
});

export default standardViewStyles;