import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from "react-router-dom";
import CssBaseline from "@material-ui/core/CssBaseline";
import { ThemeProvider } from "@material-ui/core/styles";
//
import dark from "./assets/jss/themes/dark"
import App from './App';

ReactDOM.render(
    <BrowserRouter>
        <ThemeProvider theme={dark}>
            <CssBaseline />
            <App />
        </ThemeProvider>
    </BrowserRouter>, document.getElementById("root")
);