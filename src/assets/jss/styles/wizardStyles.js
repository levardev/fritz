const wizardStyles = theme => ({
    lowerPaper: {
        color: "white",
        backgroundColor: theme.palette.primary.dark,
        margin: theme.spacing(2),
        alignItems: "center",
        display: 'flex',
        flexDirection: 'column'
    },
    boxPaper: {
        color: "white",
        backgroundColor: "dark gray",
        margin: theme.spacing(2)
    },
    progressDotsHolder: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: "center",
        margin: theme.spacing(2)
    },
    fixedModal: {
        display: 'flex',
        flexDirection: 'column',
        height: '100%',
        position: "fixed",
        alignItems: "center",
        width: "100vw",
        overflowY: "auto",
        margin: theme.spacing(2)
    },
    qrBorder: {
        margin: theme.spacing(2),
        padding: theme.spacing(2),
        color: "white",
        backgroundColor: "white",
        overflow: "hidden",
    },
    progressDots: {
        maxWidth: 400,
        flexGrow: 1
    },
    spacer: {
        margin: theme.spacing(2)
    },
    spacerMini: {
        margin: theme.spacing(0.25)
    },
    viksDebut: {
        display: 'flex',
        marginTop: "10vh",
        height: '320px',
        width: '320px'
    }
});

export default wizardStyles;