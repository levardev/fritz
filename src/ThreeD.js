import React, { useState, useRef } from "react";
import { Canvas, useFrame } from "@react-three/fiber"
import { makeStyles } from "@material-ui/core/styles";
import { Paper, Typography } from '@material-ui/core';
import wizardStyles from "./assets/jss/styles/wizardStyles";

const useStyles = makeStyles(wizardStyles);

const ThreeD = () => {
    const classes = useStyles();

    const Box = (props) => {
        const [active, setActive] = useState(false);
        const ref = useRef();
        useFrame(() => {ref.current.rotation.x += 0.01; ref.current.rotation.y += 0.01});
        return (
          <mesh
            {...props}
            ref={ref}
            scale={active ? 2.5 : 1}
            onClick={(event) => setActive(true)}>
            <boxGeometry args={[1.5, 1.5, 1.5]} />
            <meshStandardMaterial color={active ? 'green' : 'gray'} />
          </mesh>
        );
    };

    return (
        <Paper className={classes.lowerPaper}>
            <div className={classes.viksDebut}>
                <Canvas>
                    <ambientLight />
                    <pointLight position={[10, 10, 10]} />
                    <Box position={[0, 0, 0]} />
                </Canvas>
            </div>
            <Typography className={classes.spacer}>Try to tap on the cube to turn it green. When you are done, return to the problem report tab in your browser</Typography>
        </Paper>
    );
};

export default ThreeD;